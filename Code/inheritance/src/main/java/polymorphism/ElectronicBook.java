package polymorphism;

public class ElectronicBook extends Book {
    private double numberBytes;

    public ElectronicBook(String title, String author, double number) {
        super(title, author);
        this.numberBytes = number;
    }

    public double getNumberBytes() {
        return this.numberBytes;
    }

    public String toString() {
        String fromBook = super.toString();
        return fromBook + ". The number of byte is: " + this.numberBytes;
    }
}
