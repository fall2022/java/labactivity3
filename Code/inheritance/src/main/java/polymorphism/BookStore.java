package polymorphism;

public class BookStore {
    
    public static void main(String[] args) {
        
        Book[] bookList = new Book[5];

        bookList[0] = new Book("Hunger Games", "Suzanne Collins");
        bookList[1] = new ElectronicBook("The Lightning Thief", "Rick Riordan", 12);
        bookList[2] = new Book("The Institute", "Stephen King");
        bookList[3] = new ElectronicBook("Doctor Sleep", "Stephen King", 15);
        bookList[4] = new ElectronicBook("Dune", "Frank Herbert", 27);

        for (int i=0; i<bookList.length; i++) {
            System.out.println( bookList[i] );
        }
    }
}
