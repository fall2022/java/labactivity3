package polymorphism;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {
    final double TOLERANCE = .0000001;

    @Test
    public void testGetMethods() {
        Book bTest = new Book("Hello", "World");

        assertEquals("Hello", bTest.getTitle());
        assertEquals("World", bTest.getAuthor());
    }

    @Test
    public void testToString() {
        Book bTest = new Book("Good", "Morning");
        String expected = "The title is: Good and the author is: Morning";

        assertEquals(expected, bTest.toString());
    }
}
