//https://gitlab.com/fall2022/java/labactivity3.git
package polymorphism;

public class Book {
    
    protected String title;
    private String author;

    public Book(String title, String author) {
        this.title = title;
        this.author = author;

    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public String toString() {
        return "The title is: " + this.title + " and the author is: " + this.author;
    }
}
